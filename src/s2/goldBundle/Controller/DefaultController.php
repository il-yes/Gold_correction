<?php

namespace s2\goldBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use s2\goldBundle\Entity\Comment;
use s2\goldBundle\Form\CommentType;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    public function indexAction()
    {
        $repository = $this
            ->getDoctrine()
            ->getManager()
            ->getRepository('s2goldBundle:Comment');

        $aComment = $repository->findAll();

        return $this->render('s2goldBundle:Default:index.html.twig', array('comments' => $aComment));
    }

    public function formAction(Request $request)
    {

        $oNewComment = new Comment();
        $form = $this->createForm(new CommentType, $oNewComment);

        $form->handleRequest($request);

        if ($form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($oNewComment);
            $em->flush();

            return $this->redirect($this->generateUrl('s2gold_homepage'));

        }

        return $this->render('s2goldBundle:Default:form.html.twig', array(
                'form' => $form->createView(),
            )
        );
    }

    public function deleteAction($id)
    {
        $repository = $this
            ->getDoctrine()
            ->getManager()
            ->getRepository('s2goldBundle:Comment');
        $oComment =  $repository->find($id);

        $em = $this->getDoctrine()->getManager();
        $em->remove($oComment);
        $em->flush();

        return $this->redirect($this->generateUrl('s2gold_homepage'));
    }
}
