<?php
namespace s2\goldBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use s2\goldBundle\Entity\Comment;

class LoadUserData implements FixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {


        for ($i = 0; $i < 100; $i++) {
            $comment = new Comment();
            $comment->setText(" Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent non scelerisque ex, at fermentum neque. Curabitur sollicitudin metus ipsum, non vulputate arcu scelerisque placerat. Nam hendrerit urna eu fringilla fermentum. Vivamus finibus lorem ac elit ornare scelerisque. In tempus justo odio. Suspendisse at erat nec velit euismod venenatis. Aenean dictum feugiat efficitur. Maecenas placerat tincidunt lacus. Donec pretium nibh eu fermentum volutpat. Sed bibendum ultrices urna, nec ornare metus placerat a.

Mauris malesuada, tortor ac condimentum interdum, quam elit tempor libero, vitae finibus augue mi eget erat. Aliquam dignissim, mi et faucibus iaculis, mauris nisi ornare dolor, eget facilisis libero nibh sit amet erat. In lacinia egestas neque vel consequat. Suspendisse eget mauris in ex cursus laoreet. Etiam vehicula condimentum diam, non semper lectus. Mauris laoreet nunc vitae gravida aliquet. Nullam at convallis sem. Aenean tempus finibus risus, sed dictum quam aliquam id. Donec non urna in mi pharetra efficitur. Donec a ultricies ipsum, vel maximus leo. Maecenas volutpat vulputate nulla. Nulla interdum dapibus metus ornare commodo. Maecenas vitae condimentum orci, quis mollis quam. Morbi eget ex eleifend, tempus metus ut, ultricies erat. Aenean rhoncus mauris purus, vitae faucibus velit pretium a. Nunc fringilla scelerisque leo nec mattis. ");

            $manager->persist($comment);
            $manager->flush();

        }


    }
}